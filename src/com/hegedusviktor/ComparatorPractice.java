package com.hegedusviktor;

import java.util.*;

class Student {
    private int id;
    private String fname;
    private double cgpa;
    public Student(int id, String fname, double cgpa) {
        super();
        this.id = id;
        this.fname = fname;
        this.cgpa = cgpa;
    }
    public int getId() {
        return id;
    }
    public String getFname() {
        return fname;
    }
    public double getCgpa() {
        return cgpa;
    }
}

//Complete the code
public class ComparatorPractice
{
    public static void main(String[] args){
        List<Student> studentList = new ArrayList<>();
        studentList.add(new Student(1, "a", 10d));
        studentList.add(new Student(2, "d", 12d));
        studentList.add(new Student(3, "b", 9d));
        studentList.add(new Student(4, "c", 6d));

        Collections.sort(studentList, (o1, o2) -> {
            if (o1.getCgpa() == o2.getCgpa()) {
                if (o1.getFname().equals(o2.getFname())) {
                    return o1.getId() - o2.getId();
                }
                return o1.getFname().compareTo(o2.getFname());
            }
            if ((o2.getCgpa() - o1.getCgpa()) < 0) return -1;
            return 1;
        });

//        studentList.sort(Comparator.comparing(Student::getCgpa).reversed()
//                .thenComparing(Student::getFname)
//                .thenComparing(Student::getId));

        for(Student st: studentList){
            System.out.println(st.getFname());
        }
    }
}



