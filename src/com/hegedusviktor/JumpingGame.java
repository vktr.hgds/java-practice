package com.hegedusviktor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class JumpingGame {
    public static void main(String[] args) {
        int[] game = {0, 0, 1, 1, 1, 0};
        int leap = 3;
        System.out.println(canWin(leap, game));
    }

    private static boolean canWin(int leap, int[] game) {
        List<List<Integer>> lands = findLands(game);
        if (lands.size() == 1) return true;

        int currentLand = 0;
        for (int i = 0; i < lands.size() - 1; i++) {
            for (int landPosition : lands.get(i)) {
                if (lands.get(i + 1).contains(landPosition + leap)) {
                    currentLand = i + 1;
                    break;
                }
            }
        }

        if (currentLand != lands.size() - 1) return false;
        int maxPosition = Collections.max(lands.get(currentLand));
        if (maxPosition + leap > game.length - 1) return true;
        return maxPosition + leap == game.length - 1 && game[game.length - 1] == 0;
    }

    private static List<List<Integer>> findLands(int[] game) {
        List<List<Integer>> lands = new ArrayList<>();

        int k = 0;
        while (k < game.length) {
            if (game[k] == 1) {
                k++;
                continue;
            }
            List<Integer> currentLand = new ArrayList<>();
            while (game[k] == 0) {
                currentLand.add(k);
                k++;
                if (k == game.length) break;
            }
            lands.add(currentLand);
        }

        return lands;
    }

}
