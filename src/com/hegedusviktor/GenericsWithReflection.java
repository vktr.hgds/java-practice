package com.hegedusviktor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

class Printer {
    <T> void printArray(T[] elements) {
        Arrays.stream(elements).forEach(System.out::println);
    }

}

public class GenericsWithReflection {

    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        Integer[] intArray = { 1, 2, 3 };
        String[] stringArray = {"Hello", "World"};
        int count = 0;

        for (Method method : Printer.class.getDeclaredMethods()) {
            String name = method.getName();
            if (name.equals("printArray"))
                count++;
        }

        if (count > 1) System.out.println("Method overloading is not allowed!");
        else {
            Class<?> c = Class.forName("com.hegedusviktor.Printer");
            Object obj = c.newInstance();
            Method method = obj.getClass().getDeclaredMethod("printArray", Object[].class);
            method.invoke(obj, (Object) intArray);
            method.invoke(obj, (Object) stringArray);
        }

    }
}