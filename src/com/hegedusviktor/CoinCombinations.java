package com.hegedusviktor;

import java.util.Arrays;

public class CoinCombinations {
    public static void main(String[] args) {
        int[] coins = {1, 2};
        int[] coins2 = {1, 2, 5};
        System.out.println(calculateCombo(coins, 4, 0));
        System.out.println(calculateCombo(coins, 4));
        System.out.println(calculateMinCombo(coins2, 11));
    }

    private static int calculateCombo(int[] coins, int totalAmount, int currentIndex) {
        if (totalAmount == 0) return 1;
        if (totalAmount < 0) return 0;

        int comboCount = 0;
        for (int i = currentIndex; i < coins.length; i++) {
            comboCount += calculateCombo(coins, totalAmount-coins[i], currentIndex);
        }
        return comboCount;
    }

    private static int calculateCombo(int[] coins, int totalAmount) {
        if (coins.length == 0) return 0;
        if (totalAmount <= 0) return 0;

        int[] ways = new int[totalAmount + 1];
        Arrays.fill(ways, 0);
        ways[0] = 1;

        for (int coin : coins) {
            for (int j = 0; j < ways.length; j++) {
                if (j >= coin) {
                    ways[j] += ways[j - coin];
                }
            }
        }
        return ways[totalAmount];
    }

    private static int calculateMinCombo(int[] coins, int totalAmount) {
        if (coins.length == 0) return 0;
        if (totalAmount <= 0) return 0;

        int[] ways = new int[totalAmount + 1];
        Arrays.fill(ways, totalAmount + 1);
        ways[0] = 0;

        for (int j = 1; j < ways.length; j++) {
            for (int coin : coins) {
                if (j >= coin) {
                    ways[j] = Math.min(1 + ways[j - coin], ways[j]);
                }
                printSolution(ways);
            }
        }

        return ways[totalAmount];
    }

    private static void printSolution(int[] ways) {
        Arrays.stream(ways).forEach(val -> System.out.print(val + " - "));
        System.out.println();
    }
}
