package com.hegedusviktor;

import java.util.Set;

public class Main {

    public static void main(String[] args) {
        showResult(10, "1");
    }

    private static String lookAndSay(String number) {
        if (number == null || number.isEmpty() || !(number.matches("[0-9]+"))) return "";

        StringBuilder stringBuilder = new StringBuilder();
        int currentCharPos = 0;

        for (int i = 0; i < number.length(); i++) {
            if (number.charAt(currentCharPos) != number.charAt(i)) {
                String totalDigits = number.substring(currentCharPos, i);
                stringBuilder.append(totalDigits.length()).append(number.charAt(currentCharPos));
                currentCharPos = i;
            }
        }

        stringBuilder
                .append(number.substring(currentCharPos, number.length()).length())
                .append(number.charAt(currentCharPos));

        return stringBuilder.toString();
    }

    private static void showResult(int loopCount, String number) {
        for (int i = 0; i < loopCount; i++) {
            number = lookAndSay(number);
            System.out.println(i + ": " + number);
        }
    }
}
